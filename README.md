Built using [`blogdown`](https://bookdown.org/yihui/blogdown/) on top of [Hugo](https://gohugo.io/).

Deployed on homelab server, served via nginx.

Theme: [etch](https://github.com/LukasJoswiak/etch)

## Usage instructions

The `etch` theme does not support rendering of `.Rmd` documents.
Hence, each post which requires R code to be rendered is knitted first to `.md`.
When using `.Rmd` directly, syntax highlighting would not work.

NOTE: On a new local clone, do the following to properly init the theme:

```
git submodule update
git submodule init
```

## Images

For code which produces images, post-processing is needed with respect to file paths.
The code is stored in the `render.R` file of the respective post.
Example:

```
# fix path of figures created by code
system("gsed -i 's#content/posts#/posts#g' content/posts/2016-11-01-oddsratio/2016-11-01-oddsratio.md")
```

## Auto-dark mode

The [`etch` theme](https://github.com/LukasJoswiak/etch) comes with an automatic auto-dark mode which respects system settings.

## New post

```r
blogdown::new_post("Transitioning from x86 to arm64 on macOS- a use case for R users",  tags = "R", categories = "R", ext = ".md", subdir = "posts")
```

## Anchors for headers

- Add `layouts/_default/_markup/render-heading.html`

Style:

```
main#content a.hash-link.nohover {
    text-decoration: none;
}
```

Source: https://natclark.com/tutorials/hugo-linked-headers/
