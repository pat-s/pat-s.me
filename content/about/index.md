---
title: "About"
menu:
  main:
    weight: 20
---

I am a german-born developer who likes to write about all kinds of tech-related things.
I started out with R initially and enjoy spending time in devops-related topics lately.

I graduated in Geoinformatics (2016) at Friedrich-Schiller-University Jena and finished a PhD in machine learning in 2022.
A publication list can be found on [Google Scholar](https://scholar.google.de/citations?hl=de&view_op=list_works&gmla=AJsN-F4JvXCFuAl6azH2BpgGWL80qcs7yaExBIqEGMHQLg8WD5aDM5ZS3SHpnfv6THwdzWSdeGxe-mAZhCX05SeCuhcAVz451OZqesS2jaRN8FrVAAWGUwffLfCqFHcwFC1YaDWq6FGe0IFEB0eHmDMebAaHd-8wbQ&user=-gl9va8AAAAJ).

I contribute to various open-source projects, if time permits.
Keeping a comprehensive list would be too time intense but [my GitHub profile](https://github.com/pat-s) should be a good starting point.
Lately I've moved over some repos to [Codeberg](https://codeberg.org/pat-s).
I am also contributing to the development of [Gitea](https://gitea.io/en-us/), [Woodpecker CI](https://woodpecker-ci.org/) and have been a core member of the [mlr-org](https://github.com/mlr-org) between 2017 - 2022.

## GPG

I currently have two GPG keys:

- Fingerprint: `D75C 4F1B 6EE2 C8CD 3727  D142 3C63 1884 1EF7 8925`. Verified at https://keyserver.ubuntu.com/.
- Fingerprint: `AAC690DCD5A1FEB645C3C79F81271241A44A9A69`. Verified at https://keys.openpgp.org.
