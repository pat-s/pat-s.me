---
title: "Home"
---

👋️ Hey, welcome to my personal website and blogging space.

To comment on a post, you can use the comment box below each post.
This feature is powered by the FOSS project [remark42](https://remark42.com/).
Comments can also be done anonymously if you don't want to use your GitHub account or don't have one.

An overview of my personal projects can be found in [projects](./projects).
If the posts helped you to save some time and/or prevent anger, you can [buy me a coffee 🔗️](https://www.buymeacoffee.com/patrickschratz).

{{% fontawesome github %}} [pat-s](https://github.com/pat-s) <a href="https://codeberg.org/pat-s"> <img style="vertical-align: middle; padding-right: 3px;" width="15" height="15" src="https://design.codeberg.org/logo-kit/icon.svg" aria-hidden="true">pat-s</a>

