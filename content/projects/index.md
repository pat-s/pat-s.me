---
title: "Projects"
menu:
  main:
    weight: 20
---

This is a static list of my personal projects.
It is probably not complete but might give a more concise overview than solely looking at my [GitHub profile](https://github.com/pat-s).

## Workflow tools

- [alfred-gitea](https://github.com/pat-s/alfred-gitea) - Gitea Alfred Workflow
- [alfred-open-with-vscodium](https://github.com/pat-s/alfred-open-with-vscodium) - Open a directory in VSCodium with Alfred
- [alfred-r](https://github.com/pat-s/alfred-r) - #rstats Alfred workflow


## R

### Packages

- [mlr](https://github.com/mlr-org/mlr) - Machine Learning in R
- [mlr3filters](https://github.com/mlr-org/mlr3filters) - Filter-based feature selection for mlr3
- [mlr3learners](https://github.com/mlr-org/mlr3learners) - Recommended learners for mlr3
- [mlr3spatiotempcv](https://github.com/mlr-org/mlr3spatiotempcv) - Spatiotemporal resampling methods for mlr3
- [oddsratio](https://github.com/pat-s/oddsratio) - Simplified odds ratio calculation of binomial GAM/GLM models
- [raddins](https://github.com/pat-s/raddins) - Personal R function and RStudio addins
- [tic](https://github.com/ropensci/tic) - Tasks Integrating Continuously: CI-Agnostic Workflow Definitions

### Misc

- [rcli](https://github.com/pat-s/rcli) - Command line tool to install and switch between R versions
- [Supercharging RStudio](https://www.notion.so/pat-s/Supercharging-RStudio-3d17d0b4642f43cb871227460d7b74b7) - Post showing possibly tweaks to improve the user experience for RStudio users
- [xaringan-metropolis](https://github.com/pat-s/xaringan-metropolis) - Metropolis theme for {xaringan} presentation
- [quarto-metropolis](https://codeberg.org/pat-s/quarto-metropolis) - Metropolis theme for `quarto` presentations

## Other

- [dotfiles](https://github.com/pat-s/dotfiles) - My dotfiles, managed via `yadm`
- [gitea-github-theme](https://github.com/pat-s/gitea-github-theme) - Opinionated GitHub-based light and dark themes for Gitea
- [ppcryptoparser](https://github.com/pat-s/ppcryptoparser) - Crypto currency staking rewards parser for Portfolio Performance
