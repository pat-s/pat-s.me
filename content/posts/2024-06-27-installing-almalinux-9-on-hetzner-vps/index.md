---
title: Installing AlmaLinux 9 on Hetzner VPS
author: Patrick Schratz
date: "2024-06-27"
slug: installing-almalinux-9-on-hetzner-vps
categories:
  - linux
tags:
  - "Hetzner"
  - "VPS"
  - "AlmaLinux"
---

## Background

I recently ordered an [AX42 VPS from Hetzner](https://www.hetzner.com/dedicated-rootserver/matrix-ax/).
The idea was to go with AlmaLinux 9 as the OS.
When starting off, I realized Hetzner only provides installers for AlmaLinux 8.

After the first install, I upgraded to Alma 9 and quickly realized that there might be a reason for the limitations: the VPS didn't boot properly anymore.
I did some research and found that there is an [issue](https://github.com/hetzneronline/installimage/issues/57) with `grub2` in AlmaLinux 9 (at least with Hetzner's images/networking).
Although Hetzner provides [Alma 9 installers in general](https://docs.hetzner.com/robot/dedicated-server/operating-systems/standard-images/), they prevent you from installing Alma 9 when attempting to do so via `installimage` using their [Rescue System](https://docs.hetzner.com/robot/dedicated-server/operating-systems/installimage/).

Searching further, I came across [this thread in the AlmaLinux forums](https://forums.almalinux.org/t/upgrade-8-8-to-9-2-using-leapp-does-not-boot-into-elevate-upgrade-initramfs) where some users gave some instructions/hint how the initial Alma 9 installation could work.
Yet this is only one part of the puzzle as afterwards you need to ensure that the OS also boots properly (which it won't if you do an upgrade from 8 -> 9).

In the following I am sharing detailed instructions how I managed to install Alma 9 on my Hetzner VPS that also survives reboots.

## Instructions

Important: the whole approach will wipe all your existing data on the disk.
You should only execute it on a new server (or if you're OK with starting over).

1. [Boot into the Rescue System](https://docs.hetzner.com/robot/dedicated-server/troubleshooting/hetzner-rescue-system)
2. The rescue system contains all Hetzner image installers. Yet we cannot simply use the Alma 9 installer directly as Hetzner will prevent as it is not officially supported on AX42.
   Hence, we need to rename/link the Alma 9 installer to Alma 8 and with that, trick `installimage` into allowing the installation.
   _(The exact filenames might differ in your case depending on when exactly you are reading this post, i.e. there might be newer base images in place.)_

   ```sh
   mkdir images9
   cp /root/.oldroot/nfs/images/Alma-92-amd64-base.tar.gz images9/
   cp /root/.oldroot/nfs/images/Alma-92-amd64-base.tar.gz.sig images9/
   cd

   mv images9/Alma-92-amd64-base.tar.gz images9/Alma-89-amd64-base.tar.gz
   mv images9/Alma-92-amd64-base.tar.gz.sig images9/Alma-89-amd64-base.tar.gz.sig

   ln -s images9/Alma-89-amd64-base.tar.gz images9/Alma-8-latest-amd64-base.tar.gz
   rm  /root/images
   ln -s images9/ /root/images
   installimage
   ```

3. When running `installimage` you will be asked to choose the OS version.
   Select AlmaLinux 8. Next, you an editor will open that allows you change some settings.
   At the bottom of the file, there is a line which references the installer image to be used.
   Change that line to `IMAGE /root/images9/Alma-89-amd64-base.tar.gz` (this is in fact the Alma 9 installer from the previous step).

4. After the installation has finished, the boot order needs to be adjusted as otherwise the server will not boot into Alma 9 by default.
   Run `efibootmgr`, the output should look like this:

   ```sh
   BootCurrent: 0002
   Timeout: 5 seconds
   BootOrder: 0002,0003,0004,0005,0001
   Boot0001  UEFI: Built-in EFI Shell
   Boot0002* UEFI: PXE IP4 P0 Intel(R) I210 Gigabit  Network Connection
   Boot0003* UEFI OS
   Boot0004* UEFI OS
   Boot0005* almalinux
   ```

   You notice that the current boot uses `0002` (the number might be different for you).
   It must be changed so that the number that references `almalinux` comes first (the order after the first one does not matter):

   ```sh
   efibootmgr -o 0005,0002,0001,00004,0003
   ```

5. Reboot

6. You should be able to login after the reboot and find yourself within an AlmaLinux 9 OS.
   Before doing anything else, the next step is to exclude `grub2` from being updated by `dnf` as this will again cause boot failures.
   This issue might resolve itself at some point in the future, but for now, there is no other way than to exclude it.
   Call `echo "exclude=grub2-*" >> /etc/yum.conf` and then run `dnf update` to update the system (the installer image is usually quite old, in my case it was based on 9.2 whereas 9.4 was already out).

7. Reboot again and verify that the server is still booting properly.
8. Be happy!

<script>
  !(function (e, n) {
    for (var o = 0; o < e.length; o++) {
      var r = n.createElement("script"),
        c = ".js",
        d = n.head || n.body;
      "noModule" in r ? ((r.type = "module"), (c = ".mjs")) : (r.async = !0),
        (r.defer = !0),
        (r.src = remark_config.host + "/web/" + e[o] + c),
        d.appendChild(r);
    }
  })(remark_config.components || ["embed"], document);
</script>

<div id="remark42"></div>
