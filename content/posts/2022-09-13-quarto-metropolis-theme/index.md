---
title: Quarto - Metropolis theme
author: Patrick Schratz
date: '2022-09-13'
slug: quarto-metropolis-theme
categories:
  - R
tags:
  - R
  - quarto
  - metropolis
  - theme
---

As a fan of the [Metropolis beamer theme](https://github.com/matze/mtheme) (by Matthias Vogelsang) I've created a theme port for the use within {xaringan} a few years ago ([pat-s/xaringan-metropolis](https://github.com/pat-s/xaringan-metropolis)).
Fast-forward to today, [quarto](https://quarto.org/) is the new kid on the block when it comes to presentations in R.
Recently it was time for my first presentation in the `quarto` era and I used the opportunity to also create a theme port of the Metropolis theme for `quarto`.

Compared to the orignal {xaringan} port, I've tweaked a few things:

- background colors
- code highlighting
- border radius
- column background coloring
- default fonts

The code of the theme is hosted on [Codeberg](https://codeberg.org/), a FOSS alternative to GitHub and GitLab:

https://codeberg.org/pat-s/quarto-metropolis

Here are some screenshots:

{{< gallery match="images/*" loadJQuery=true sortOrder="desc" rowHeight="150" margins="5" thumbnailResizeOptions="600x600 q90 Lanczos" showExif=true previewType="blur" embedPreview="true" >}}

<script>
  !(function (e, n) {
    for (var o = 0; o < e.length; o++) {
      var r = n.createElement("script"),
        c = ".js",
        d = n.head || n.body;
      "noModule" in r ? ((r.type = "module"), (c = ".mjs")) : (r.async = !0),
        (r.defer = !0),
        (r.src = remark_config.host + "/web/" + e[o] + c),
        d.appendChild(r);
    }
  })(remark_config.components || ["embed"], document);
</script>

<div id="remark42"></div>
