---
title: Integrating Umami into Astro website
author: Package Build
date: "2024-09-20"
slug: integrating-umami-into-astro-website
tags:
  - astro
  - umami
  - analytics
  - website
---

There are many website analytics applications available today. Aside from the dominant but privacy-concerning Google Analytics, I have been using [Umami](https://umami.is/) for some time as my personal choice.

After building my company website with [Astro](https://astro.build/), I wanted to integrate Umami into it. Normally, this is as simple as adding a `<script></script>` tag into the `<head></head>` section of the overall layout template.

### Problems

However, after doing that, I encountered two issues:

1. Visitor IPs were displayed as `(Unknown)`.
2. Visits were only recorded for the root (`/`) of the website.

The first issue usually indicates that a proxy in front of the website is masking the real IPs.

As for the second issue, I initially had no idea what was causing it. Thankfully, a helpful Astro community member provided assistance via their Discord server! (🤝)

### Solutions

To address the forwarding of real IPs, I enabled the [`proxy-protocol`](https://docs.nginx.com/nginx/admin-guide/load-balancer/using-proxy-protocol/) in the NGINX ingress. While this worked, I had to create a secondary ingress because not all applications can function with this setting (but that’s another story). It's important to mention, however, that enabling this centrally could make some or many of your applications unreachable.

For the second issue, it turned out to be caused by Astro's [`View-Transitions`](https://docs.astro.build/en/guides/view-transitions/). These are built-in page transitions designed to make page navigation smoother. However, a side effect is that they prevent a full page reload in the browser. As a result, the user is technically still on the initial page, and all subsequent transitions are treated as happening on the original page (from the browser’s perspective).

This means the analytics script doesn’t get triggered, as it doesn’t recognize that the page has changed. To resolve this, the inline script parameter `data-astro-rerun` can be used. This triggers a "real" page reload after a transition has finished, allowing analytics scripts to properly track page visits.

Here’s an example of how to implement it:

```html
<script async is:inline data-astro-rerun src="<src>" data-website-id="<id>"></script>
```

PS: Tracking visits on website is pretty standard and there's nothing bad in it (from a privacy standpoint) as long as the data stays with the website owner and is not used for commercial purposes post-collection.

<script>
  !(function (e, n) {
    for (var o = 0; o < e.length; o++) {
      var r = n.createElement("script"),
        c = ".js",
        d = n.head || n.body;
      "noModule" in r ? ((r.type = "module"), (c = ".mjs")) : (r.async = !0),
        (r.defer = !0),
        (r.src = remark_config.host + "/web/" + e[o] + c),
        d.appendChild(r);
    }
  })(remark_config.components || ["embed"], document);
</script>

<div id="remark42"></div>
