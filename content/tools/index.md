---
title: "Tools"
menu:
  main:
    weight: 20
---

The following is a non-comprehensive list of tools I use frequently.
Some of these are only available on macOS.

- Shell: [fish](https://fishshell.com/)
- Terminal: [warp](https://www.warp.dev/)
- Browser: [Arc](https://arc.net/)
- Font: [JetBrainsMono Nerd Font](https://www.jetbrains.com/lp/mono/)
- Workflow Helper: [Alfred](https://www.alfredapp.com/)
- Mail app: [MailMate](https://freron.com/)
- PDF reader: [PDF Expert](https://pdfexpert.com/)
- Calendar app: [Fantastical](https://www.fanstastical.com/)
- GUI Editor: [Zed](https://zed.dev/)
- CLI Editor: [neovim](https://neovim.io/)
- dotfiles manager: [yadm](https://yadm.io/)
- Password manager: [Vaultwarden](https://github.com/dani-garcia/vaultwarden)
- Notes: [Notion](https://www.notion.so/) and [Obsidian](https://obsidian.md/)
- Task manager: [Todoist](https://todoist.com/)
- Clean menu bar: [Bartender](https://www.macbartender.com/)
- VC noise removal: [krisp](https://krisp.ai/)
- Bibliography Manager: [Zotero](https://www.zotero.org/)
- Screenshot tool: [Zappy](https://zappy.app/)
- Resource Manager: [istatmenus](https://bjango.com/mac/istatmenus/)
- VPN: [Wireguard](https://wireguard.com)

My dotfiles are publicly available on [GitHub](https://github.com/pat-s/dotfiles).
System packages are managed via [homebrew](https://brew.sh/).
