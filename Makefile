publish-website:
	ssh -t homelab-de 'cd /home/pjs/pat-s.me && git pull && hugo && rm -rf /home/pjs/website-pub/* && mv /home/pjs/pat-s.me/public /home/pjs/website-pub/'

publish-website-local:
	cd /home/pjs/pat-s.me && hugo && rm -rf /home/pjs/website-pub/* && mv /home/pjs/pat-s.me/public /home/pjs/website-pub/

preview:
	hugo server
